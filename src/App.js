import React from 'react';
import BarbiqueImg from './images/barbeque_nation.jpeg'
import FaasosImg from './images/faasos.jpeg'
import FirangiBakeImg from './images/firangi bake.jpeg'
import GoodBowlImg from './images/good bowl.jpeg'
import MandarinoakImg from './images/mandarin oak.jpg'
import BakingoImg from './images/bakingo.jpeg'
import RoseatotelImg from './images/roseate hotal.jpeg'
import SweetTruth from './images/sweet truth.jpeg'
import StarImg from './images/star.jpeg'
import BlankStar from './images/star_blank.png'
import HeartIcon from './images/heart.png'

let baseUrl = 'http://localhost:3000/'

function App() {
  return (
    <div className="container-fluid pt-15px">
      <div className="row form-group">
        <div className="col-12 col-sm-3">
          <div className="card main-card">
            <img src={BarbiqueImg} className="card-img" alt="Barbique Img" />
            <div className="card-detail">
              <div className="d-table full-width">
                <span className="card-heading">Barbeque Nation</span>
                <span className="star-main-container">
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={BlankStar} className="star-img" alt="star" />
                  <span className="rating-text">4.4</span>
                </span>
              </div>
              <div className="location-locator">
                Pan India
              </div>
              <div className="voucher-head">
                Food Voucher
              </div>
              <div className="voucher-text">
                Buy a gift voucherworth Rs. 1800 and get two complimentary Mooktail/Beer Pint.
              </div>
              <div className="more-offer">
                1 more offer(s)
                <span className="caret"></span>
              </div>
              <div className="card-buttons">
                <img src={HeartIcon} alt="Heart" className="heart-img" />
                <a href={baseUrl + BarbiqueImg} target="_blank" rel="noopener noreferrer" className="btn buy-now-btn">Buy now</a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 col-sm-3">
          <div className="card main-card">
            <img src={FaasosImg} className="card-img" alt="Faasos" />
            <div className="card-detail">
              <div className="d-table full-width">
                <span className="card-heading">Faasos</span>
                <span className="star-main-container">
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={BlankStar} className="star-img" alt="star" />
                  <span className="rating-text">4.4</span>
                </span>
              </div>
              <div className="location-locator">
                Mumbai, Pune, Nagpur, Ahmadabad, Vadodra, Delhi, Gurugram.
              </div>
              <div className="voucher-head">

              </div>
              <div className="voucher-text">
                50% off upto Rs. 75 on min. order of Rs. 99
              </div>
              <div className="more-offer">

              </div>
              <div className="card-buttons">
                <img src={HeartIcon} alt="Heart" className="heart-img" />
                <a href={baseUrl + FaasosImg} target="_blank" rel="noopener noreferrer" className="btn buy-now-btn">Buy now</a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 col-sm-3">
          <div className="card main-card">
            <img src={FirangiBakeImg} className="card-img" alt="Firangi Bake" />
            <div className="card-detail">
              <div className="d-table full-width">
                <span className="card-heading">Firangi Bake</span>
                <span className="star-main-container">
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={BlankStar} className="star-img" alt="star" />
                  <span className="rating-text">4.4</span>
                </span>
              </div>
              <div className="location-locator">
                Mumbai, Pune, Nagpur, Ahmadabad, Vadodra, Delhi, Gurugram.
              </div>
              <div className="voucher-head">

              </div>
              <div className="voucher-text">
                Rs 150 off on min. order of Rs. 299
              </div>
              <div className="more-offer">

              </div>
              <div className="card-buttons">
                <img src={HeartIcon} alt="Heart" className="heart-img" />
                <a href={baseUrl + FirangiBakeImg} target="_blank" rel="noopener noreferrer" className="btn buy-now-btn">Buy now</a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 col-sm-3">
          <div className="card main-card">
            <img src={GoodBowlImg} className="card-img" alt="Good Bowl" />
            <div className="card-detail">
              <div className="d-table full-width">
                <span className="card-heading">Good Bowl</span>
                <span className="star-main-container">
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={BlankStar} className="star-img" alt="star" />
                  <span className="rating-text">4.4</span>
                </span>
              </div>
              <div className="location-locator">
                Mumbai, Pune, Nagpur, Ahmadabad, Vadodra, Delhi, Gurugram.
              </div>
              <div className="voucher-head">

              </div>
              <div className="voucher-text">
                50% off upto Rs. 100 on min. order of Rs. 199
              </div>
              <div className="more-offer">

              </div>
              <div className="card-buttons">
                <img src={HeartIcon} alt="Heart" className="heart-img" />
                <a href={baseUrl + GoodBowlImg} target="_blank" rel="noopener noreferrer" className="btn buy-now-btn">Buy now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row form-group">
        <div className="col-12 col-sm-3">
          <div className="card main-card">
            <img src={MandarinoakImg} className="card-img" alt="Mandarin Oak" />
            <div className="card-detail">
              <div className="d-table full-width">
                <span className="card-heading">Mandarin Oak</span>
                <span className="star-main-container">
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={BlankStar} className="star-img" alt="star" />
                  <span className="rating-text">4.4</span>
                </span>
              </div>
              <div className="location-locator">
                Pan India
              </div>
              <div className="voucher-head">
                Food Voucher
              </div>
              <div className="voucher-text">
                Buy a gift voucherworth Rs. 1800 and get two complimentary Mooktail/Beer Pint.
              </div>
              <div className="more-offer">
                1 more offer(s)
                <span className="caret"></span>
              </div>
              <div className="card-buttons">
                <img src={HeartIcon} alt="Heart" className="heart-img" />
                <a href={baseUrl + MandarinoakImg} target="_blank" rel="noopener noreferrer" className="btn buy-now-btn">Buy now</a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 col-sm-3">
          <div className="card main-card">
            <img src={SweetTruth} className="card-img" alt="Sweet Truth" />
            <div className="card-detail">
              <div className="d-table full-width">
                <span className="card-heading">Sweet Truth</span>
                <span className="star-main-container">
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={BlankStar} className="star-img" alt="star" />
                  <span className="rating-text">4.4</span>
                </span>
              </div>
              <div className="location-locator">
                Mumbai, Pune, Nagpur, Ahmadabad, Vadodra, Delhi, Gurugram.
              </div>
              <div className="voucher-head">

              </div>
              <div className="voucher-text">
                Rs 150 off on min. order of Rs. 299
              </div>
              <div className="more-offer">

              </div>
              <div className="card-buttons">
                <img src={HeartIcon} alt="Heart" className="heart-img" />
                <a href={baseUrl + SweetTruth} target="_blank" rel="noopener noreferrer" className="btn buy-now-btn">Buy now</a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 col-sm-3">
          <div className="card main-card">
            <img src={BakingoImg} className="card-img" alt="Bakingo" />
            <div className="card-detail">
              <div className="d-table full-width">
                <span className="card-heading">Bakingo</span>
                <span className="star-main-container">
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={BlankStar} className="star-img" alt="star" />
                  <span className="rating-text">4.4</span>
                </span>
              </div>
              <div className="location-locator">
                Mumbai, Pune, Nagpur, Ahmadabad, Vadodra, Delhi, Gurugram.
              </div>
              <div className="voucher-head">

              </div>
              <div className="voucher-text">
                50% off upto Rs. 75 on min. order of Rs. 99
              </div>
              <div className="more-offer">

              </div>
              <div className="card-buttons">
                <img src={HeartIcon} alt="Heart" className="heart-img" />
                <a href={baseUrl + BakingoImg} target="_blank" rel="noopener noreferrer" className="btn buy-now-btn">Buy now</a>
              </div>
            </div>
          </div>
        </div>
        <div className="col-12 col-sm-3">
          <div className="card main-card">
            <img src={RoseatotelImg} className="card-img" alt="Roseat Hotel" />
            <div className="card-detail">
              <div className="d-table full-width">
                <span className="card-heading">Roseat Hotel</span>
                <span className="star-main-container">
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={StarImg} className="star-img" alt="star" />
                  <img src={BlankStar} className="star-img" alt="star" />
                  <span className="rating-text">4.4</span>
                </span>
              </div>
              <div className="location-locator">
                Mumbai, Pune, Nagpur, Ahmadabad, Vadodra, Delhi, Gurugram.
              </div>
              <div className="voucher-head">

              </div>
              <div className="voucher-text">
                50% off upto Rs. 100 on min. order of Rs. 199
              </div>
              <div className="more-offer">

              </div>
              <div className="card-buttons">
                <img src={HeartIcon} alt="Heart" className="heart-img" />
                <a href={baseUrl + RoseatotelImg} target="_blank" rel="noopener noreferrer" className="btn buy-now-btn">Buy now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
